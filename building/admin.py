from django.contrib import admin

from .models import Tower, Level, Unit

admin.site.register(Tower)
admin.site.register(Level)
admin.site.register(Unit)
