from django.db import models

class Tower(models.Model):
  tower_name = models.CharField(max_length=200, null=True)
  class Meta:
          verbose_name = "Tower"
          verbose_name_plural = "Towers"
  def __str__(self):
        return self.tower_name

class Level(models.Model):
  level_name = models.CharField(max_length=200, null=True)
  tower = models.ForeignKey(Tower, on_delete=models.CASCADE, null=True)
  class Meta:
          verbose_name = "Level"
          verbose_name_plural = "Levels"
  def __str__(self):
        return f"{self.tower.tower_name}-{self.level_name}"

class Unit(models.Model):
  unit_name = models.CharField(max_length=200, null=True)
  level = models.ForeignKey(Level, on_delete=models.CASCADE, null=True)
  class Meta:
          verbose_name = "Unit"
          verbose_name_plural = "Units"
  def __str__(self):
        return f"{self.level.tower.tower_name}-{self.level.level_name}-{self.unit_name}"
